
public class DailyMessageWithEnums {

	String getMessage(DayOfTheWeek day) {
		switch(day) {
		case MONDAY: case WEDNESDAY: case FRIDAY:
			return "Study Day"; 
		case TUESDAY: case THURSDAY:
			return "Gym Day";
		case SATURDAY: case SUNDAY:
			return "Weekend";
		default:
			return "Invalid Day";
		}
	}
	
	public static void main(String[] args) {
		DailyMessageWithEnums dm = new DailyMessageWithEnums();
		DayOfTheWeek myDay = DayOfTheWeek.MONDAY;
		System.out.println(dm.getMessage(myDay));
	}
}

