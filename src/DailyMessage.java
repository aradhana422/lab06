
class DailyMessage {
	String getMessage(String day) {
		if(day.equals("Monday")||day.equals("Wednesday")||day.equals("Friday")) {
			return "Study Day";
		} else if(day.equals("Tuesday")||day.equals("Thursday")){
			return "Gym Day";
		} else if(day.equals("Saturday")||day.equals("Sunday")){
			return "Weekend";
		} else {
			return "Invalid Day";
		}
	}


}
