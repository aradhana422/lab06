import java.util.Arrays;
public class Calculator {
		
	    int stringLen(String str ) {
	     	return str.length();
	   }
	    
	    int sumNumbers(int[] array) {
	    	int sum =0;
	    
	    	for(int i =0; i<array.length; i++) {
	    		sum = sum + array[i];
	    	}
	    	return sum;
	    }
	    
	    public static void countArray(int arr[], int n) 
	    { 
	        boolean visited[] = new boolean[n]; 
	          
	        Arrays.fill(visited, false); 
	      
	        for (int i = 0; i < n; i++) { 
	      
	            if (visited[i] == true) 
	                continue; 
	      
	            int count = 1; 
	            for (int j = i + 1; j < n; j++) { 
	                if (arr[i] == arr[j]) { 
	                    visited[j] = true; 
	                    count++; 
	                } 
	            } 
	            
	            System.out.println(arr[i] + " " + count); 
	        } 
	    } 
	     
	}



